package th.co.truecorp.springboot.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/demo")
public class DemoSvcController {

	
	@GetMapping
	public String hello() {
		log.debug("Hello this is debug log");
		log.info("Hello world");
		return String.format("Hello, this is demo service app" );
	}
	
}
