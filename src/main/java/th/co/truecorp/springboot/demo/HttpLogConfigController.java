package th.co.truecorp.springboot.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/httplog")
public class HttpLogConfigController {
  

  @GetMapping(path="/config")
  public String getConfig() {
    return "{\"id\": \"01234\", \"thai-id\": \"01234\"}";
  }
}
