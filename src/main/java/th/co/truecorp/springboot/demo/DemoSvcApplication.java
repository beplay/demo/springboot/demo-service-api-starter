package th.co.truecorp.springboot.demo;

import java.util.TimeZone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.module.jaxb.JaxbAnnotationModule;

@SpringBootApplication
public class DemoSvcApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoSvcApplication.class, args);
	}
	
	/**
	 * Overwrite default with customized jackson object mapper
	 */
	@Bean
	@Primary
	public ObjectMapper jacksonObjectMapper() {
	    ObjectMapper objectMapper = new ObjectMapper();
	    objectMapper.setSerializationInclusion(Include.NON_NULL);
	    objectMapper.setTimeZone(TimeZone.getTimeZone("Asia/Bangkok"));
	    objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
	    objectMapper.registerModule(new JaxbAnnotationModule());
	    return objectMapper;
	}

}
